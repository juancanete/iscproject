<?php

namespace ISC\MultimediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MultimediaBundle:Default:index.html.twig', array('name' => $name));
    }
}
