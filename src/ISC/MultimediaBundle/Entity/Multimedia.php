<?php

namespace ISC\MultimediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Multimedia")
 */
class Multimedia
{
    /**
     * var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * var string $nombre
     * @ORM\Column(name="nombre", type="string", length=250)
     */
    protected $nombre;
    
    /**
     * var string $slug
     * @ORM\Column(name="slug", type="string", length=250)
     */
    protected $slug;
    
    /**
     * var string $descripcion
     * @ORM\Column(name="descripcion", type="string", length=2048)
     */
    protected $descripcion;
    
    /**
     * var datetime $fecha_creacion
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    protected $fecha_creacion;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    
    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function  setSlug ($slug)
    {
        $this->slug = $slug;
    }
    
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fecha_creacion = $fechaCreacion;
    }
    
    public function getFechaCreacion ()
    {
        return $this->fecha_creacion;
    }
    
    public function __toString() {
        return $this->nombre;
    }
}

?>
