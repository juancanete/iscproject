<?php

namespace ISC\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function portadaAction()
    {
        return $this->render('MenuBundle:Default:portada.html.twig');
    }
    
    public function contactoAction()
    {
        return $this->render('MenuBundle:Default:contacto.html.twig');
    }
    
    public function contactAction()
    {
        return $this->render('MenuBundle:Default:contact.html.twig');
    }
    
    public function construccionAction()
    {
        return $this->render('MenuBundle:Default:construccion.html.twig');
    }

    public function indexAction($name)
    {
        return $this->render('MenuBundle:Default:index.html.twig', array('name' => $name));
    }
}
