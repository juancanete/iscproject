<?php

namespace ISC\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Productos")
 */

class Productos
{
    
    /**
     *
     * @var integer $id
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @var string $nombre
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    protected $nombre;
    
    /**
     *
     * @var string $slug
     * @ORM\Column(name="slug", type="string", length=255)
     */
    protected $slug;


    /**
     *
     * @var string $descripcion
     * @ORM\Column(name="descripcion", type="string", length=2048)
     */
    protected $descripcion;
    
    /**
     *
     * @var string $genero
     * @ORM\ManyToOne(targetEntity="ISC\MenuBundle\Entity\Genero")
     */
    protected $genero;

    /**
     *
     * @var string $multimedia
     * @ORM\ManyToOne(targetEntity="ISC\MultimediaBundle\Entity\Multimedia")
     */
    protected $multimedia;
    
    /**
     * @var boolean $disponible
     * @ORM\Column(name="disponible", type="boolean")
     */
    protected $disponible;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    
    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function  setSlug ($slug)
    {
        $this->slug = $slug;
    }
    
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setDescripcion($descripcion) 
    {
        $this->descripcion = $descripcion;
    }
    
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function setGenero(\ISC\MenuBundle\Entity\Genero $genero)
    {
        $this->genero = $genero;
    }
    
    public function getGenero()
    {
        return $this->genero;
    }
    
    public function setMultimedia(\ISC\MultimediaBundle\Entity\Multimedia $multimedia)
    {
        $this->multimedia = $multimedia;
    }
    
    public function getMultimedia()
    {
        return $this->multimedia;
    }

    public function setDisponible($disponible)
    {
        $this->disponible = $disponible;
    }
    
    public function getDisponible()
    {
        return $this->disponible;
    }
    
    public function __toString() {
        return $this->nombre;
    }   
}

?>
