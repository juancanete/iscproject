<?php

/* ::frontend.html.twig */
class __TwigTemplate_2dd81bc95a9686564f93b38be96d9d6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'aside' => array($this, 'block_aside'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "<link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish-vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" type=\"text/css\">
";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "<img alt=\"Logo ISC\" src=\"/uploads/images/logo.jpg\"/>
<div>
    <ul class=\"sf-menu sf-vertical\">
        <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("portada"), "html", null, true);
        echo "\">Home</a></li>
        <li>
            <a href=\"#\">Productos</a>
            <ul>
                <li><a href=\"#\">Maquinaria Nueva</a></li>
                <li><a href=\"#\">Maquinaria Usada</a></li>
                <li><a href=\"#\">Maquinaria Especial</a></li>
            </ul>
        </li>
        <li><a href=\"#\">Localización</a></li>
        <li><a href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contacto_menu"), "html", null, true);
        echo "\">Contacto</a></li>
    </ul>
</div>
<aside id=\"aside_short\">
    
    ";
        // line 28
        $this->displayBlock('aside', $context, $blocks);
        // line 29
        echo "</aside>

";
    }

    // line 28
    public function block_aside($context, array $blocks = array())
    {
    }

    // line 33
    public function block_javascripts($context, array $blocks = array())
    {
        // line 34
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

<script type=\"text/javascript\">
    jQuery(document).ready(function(){
    jQuery('ul.sf-menu').superfish({
      animation: {height:'show'},   // slide-down effect without fade-in
      delay:     500               // 0.5 second delay on mouseout
    });
  });
     
</script>
";
    }

    public function getTemplateName()
    {
        return "::frontend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 34,  91 => 33,  86 => 28,  80 => 29,  78 => 28,  70 => 23,  57 => 13,  52 => 10,  49 => 9,  43 => 6,  39 => 5,  34 => 4,  31 => 3,  38 => 7,  35 => 6,  29 => 4,);
    }
}
