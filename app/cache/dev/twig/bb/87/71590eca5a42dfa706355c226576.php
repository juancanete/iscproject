<?php

/* MenuBundle:Default:contacto.html.twig */
class __TwigTemplate_bb8771590eca5a42dfa706355c226576 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "<link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish-vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" type=\"text/css\">
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        // line 10
        echo "    ISC - Contacto
";
    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        // line 14
        echo "<img alt=\"Logo ISC\" src=\"/uploads/images/logo.jpg\" />
<aside id=\"aside_dim\">
    <div id=\"div_dim\" style=\"position: absolute;  left: 30%;\">
        <h1 id=\"text_whiteBox\" style=\"text-align:center;\" >Ingeniería, Sistemas y Control</h1>
        <p id=\"text_whiteBox\" style=\"text-align:center;\">
            Polig. Ind. La Viñuela<br/>Calle del Mueble, 22<br/>Lucena (Córdoba)
        </p>
        <p id=\"text_whiteBox\" style=\"text-align:center;\"><strong>Juan Cañete Quesada</strong><br/>móvil: (+34)676979788
        <br/>email: juancq.isc@gmail.com</p>
    </div>
    
</aside>

";
    }

    public function getTemplateName()
    {
        return "MenuBundle:Default:contacto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 14,  56 => 13,  51 => 10,  48 => 9,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
