<?php

/* ::base.html.twig */
class __TwigTemplate_dab34ca45bfbea788bf3c4e8167425eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'id' => array($this, 'block_id'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
        <meta name=\"description\" content=\"Fabricación, venta y reparación de maquinaria
              de cualquier sector industrial (fresadoras CNC para madera, hierro, etc). La maquinaria que se fabrica en esta empresa
              es totalmente personalizada, adaptada al cliente, y en la cual se puede usar
              tecnología de todo tipo (Control Númerico - [Maquinaria CNC], pantallas táctiles, autómatas, etc).\">
        <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"/uploads/images/logo_ICO.ico\" />
        <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/js/superfish.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
        <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/js/hoverIntent.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
    </head>
    <body id=\"";
        // line 17
        $this->displayBlock('id', $context, $blocks);
        echo "\">
        <div id=\"contenedor\">
            ";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "
            ";
        // line 21
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "        </div>
        <footer>
            &copy; ";
        // line 24
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Ingeniería, Sistemas y Control
            <a href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contacto"), "html", null, true);
        echo "\">Contacto</a>
        </footer>
    </body>
</html>
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 17
    public function block_id($context, array $blocks = array())
    {
        echo "";
    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
    }

    // line 21
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 21,  102 => 19,  96 => 17,  91 => 10,  86 => 9,  77 => 25,  73 => 24,  69 => 22,  67 => 21,  64 => 20,  62 => 19,  57 => 17,  52 => 15,  48 => 14,  44 => 13,  40 => 11,  38 => 10,  34 => 9,  24 => 1,);
    }
}
