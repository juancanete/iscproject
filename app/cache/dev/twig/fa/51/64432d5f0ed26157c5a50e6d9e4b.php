<?php

/* MenuBundle:Default:portada.html.twig */
class __TwigTemplate_fa5164432d5f0ed26157c5a50e6d9e4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::frontend.html.twig");

        $this->blocks = array(
            'id' => array($this, 'block_id'),
            'aside' => array($this, 'block_aside'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::frontend.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_id($context, array $blocks = array())
    {
        echo "portada";
    }

    // line 6
    public function block_aside($context, array $blocks = array())
    {
        // line 7
        echo "<p id=\"text_whiteBox\">Empresa de fabricación, venta y reparación de maquinaria industrial (fresadoras CNC para madera, hierro, etc). La maquinaria industrial que se fabrica es totalmente adaptable a las necesidades del cliente y se puede enfocar a cualquier ámbito industrial (carpintería, metalurgia, etc).</p> 
";
    }

    public function getTemplateName()
    {
        return "MenuBundle:Default:portada.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 7,  35 => 6,  29 => 4,);
    }
}
