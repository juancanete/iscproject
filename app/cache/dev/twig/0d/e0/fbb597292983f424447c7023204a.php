<?php

/* MenuBundle:Default:contact.html.twig */
class __TwigTemplate_0de0fbb597292983f424447c7023204a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::frontend.html.twig");

        $this->blocks = array(
            'id' => array($this, 'block_id'),
            'title' => array($this, 'block_title'),
            'aside' => array($this, 'block_aside'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::frontend.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_id($context, array $blocks = array())
    {
        echo "portada";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        // line 7
        echo "    ISC - Contacto
";
    }

    // line 10
    public function block_aside($context, array $blocks = array())
    {
        // line 11
        echo "<div id=\"div_dim\" style=\"position: absolute;  left: 30%;\">
        <h1 id=\"text_whiteBox\" style=\"text-align:center;\" >Ingeniería, Sistemas y Control</h1>
        <p id=\"text_whiteBox\" style=\"text-align:center;\">
            Polig. Ind. La Viñuela<br/>Calle del Mueble, 22<br/>Lucena (Córdoba)
        </p>
        <p id=\"text_whiteBox\" style=\"text-align:center;\"><strong>Juan Cañete Quesada</strong><br/>móvil: (+34)676979788
        <br/>email: juancq.isc@gmail.com</p>
</div>
    
    
";
    }

    public function getTemplateName()
    {
        return "MenuBundle:Default:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 11,  44 => 10,  39 => 7,  36 => 6,  30 => 4,);
    }
}
