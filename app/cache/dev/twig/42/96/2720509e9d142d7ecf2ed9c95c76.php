<?php

/* MenuBundle:Default:construccion.html.twig */
class __TwigTemplate_42962720509e9d142d7ecf2ed9c95c76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "<link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish-vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" type=\"text/css\">
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo " Ingeniería, Sistemas y Control";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<img alt=\"Logo ISC\" src=\"/uploads/images/logo.jpg\" />
<aside id=\"aside_dim\">
    <div id=\"div_dim\" style=\"position: absolute;  left: 30%;\">
        <h1 id=\"text_whiteBox\" style=\"text-align:center;\" >Ingeniería, Sistemas y Control</h1>
        <h4 id=\"text_whiteBox\" style=\"text-align:justify;\">Empresa de fabricación,
        venta y reparación de maquinaria industrial (fresadoras CNC para madera, hierro, etc). 
        La maquinaria industrial que se fabrica es totalmente adaptable a las necesidades 
        del cliente y se puede enfocar a cualquier ámbito industrial (carpintería, metalurgia, etc).
        </h4>
        <h2 id=\"text_whiteBox\" style=\"text-align:center;\" >Página web en construcción</h2>
        <h4 id=\"text_whiteBox\" style=\"text-align:justify;\">Esta página web está en construcción. 
            La página web estará pronto disponible. En caso de que quiera contactar 
        con nosotros puede hacerlo en: </h4>
        <h4 style=\"position: absolute; left: 110px; color: #0101DF; text-align: center;\">telf: (+34)676979788 <br/> email: juancq.isc@gmail.com</h4>
        <img style=\"position: absolute; left: 10px;\" alt=\"Gif Robot\" src=\"/uploads/images/arm_med_white.gif\">
    </div>
    
</aside>

";
    }

    public function getTemplateName()
    {
        return "MenuBundle:Default:construccion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 12,  54 => 11,  48 => 9,  42 => 6,  38 => 5,  33 => 4,  30 => 3,);
    }
}
