<?php

namespace ISC\MultimediaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class DefaultController__JMSInjector
{
    public static function inject($container) {
        $instance = new \ISC\MultimediaBundle\Controller\DefaultController();
        return $instance;
    }
}
