<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        if (0 === strpos($pathinfo, '/')) {
            // productos_homepage
            if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'ISC\\ProductosBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'productos_homepage'));
            }

            // multimedia_homepage
            if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'ISC\\MultimediaBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'multimedia_homepage'));
            }

            // menu_homepage
            if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'ISC\\MenuBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'menu_homepage'));
            }

            // portada
            if ($pathinfo === '/portada') {
                return array (  '_controller' => 'ISC\\MenuBundle\\Controller\\DefaultController::portadaAction',  '_route' => 'portada',);
            }

            // contacto
            if ($pathinfo === '/contacto') {
                return array (  '_controller' => 'ISC\\MenuBundle\\Controller\\DefaultController::contactoAction',  '_route' => 'contacto',);
            }

            // construccion
            if (rtrim($pathinfo, '/') === '') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'construccion');
                }

                return array (  '_controller' => 'ISC\\MenuBundle\\Controller\\DefaultController::construccionAction',  '_route' => 'construccion',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
