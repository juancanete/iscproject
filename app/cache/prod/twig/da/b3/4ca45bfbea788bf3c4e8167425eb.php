<?php

/* ::base.html.twig */
class __TwigTemplate_dab34ca45bfbea788bf3c4e8167425eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'id' => array($this, 'block_id'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
        <meta name=\"description\" content=\"Fabricación, venta y reparación de maquinaria
              de cualquier sector industrial (fresadoras CNC para madera, hierro, etc). La maquinaria que se fabrica en esta empresa
              es totalmente personalizada, adaptada al cliente, y en la cual se puede usar
              tecnología de todo tipo (Control Númerico - [Maquinaria CNC], pantallas táctiles, autómatas, etc).\">
        <title>";
        // line 9
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"/uploads/images/logo_ICO.ico\" />
        <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.9.1.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/js/superfish.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
        <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/js/hoverIntent.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script> 
    </head>
    <body id=\"";
        // line 17
        $this->displayBlock('id', $context, $blocks);
        echo "\">
        <div id=\"contenedor\">
            ";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "
            ";
        // line 21
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "        </div>
        <footer>
            &copy; ";
        // line 24
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Ingeniería, Sistemas y Control
            <a href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("contacto"), "html", null, true);
        echo "\">Contacto</a>
        </footer>
    </body>
</html>
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 17
    public function block_id($context, array $blocks = array())
    {
        echo "";
    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
    }

    // line 21
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 9,  77 => 25,  69 => 22,  62 => 19,  52 => 15,  40 => 11,  56 => 13,  479 => 162,  473 => 161,  468 => 158,  460 => 155,  456 => 153,  452 => 151,  443 => 149,  439 => 148,  436 => 147,  434 => 146,  429 => 144,  426 => 143,  422 => 142,  412 => 134,  408 => 132,  406 => 131,  401 => 130,  397 => 129,  392 => 126,  386 => 122,  383 => 121,  380 => 120,  378 => 119,  373 => 116,  367 => 112,  364 => 111,  361 => 110,  359 => 109,  354 => 106,  340 => 105,  336 => 103,  321 => 101,  313 => 99,  311 => 98,  308 => 97,  304 => 95,  297 => 91,  293 => 90,  284 => 89,  282 => 88,  277 => 86,  267 => 85,  263 => 84,  257 => 81,  251 => 80,  246 => 78,  240 => 77,  234 => 74,  228 => 73,  223 => 71,  219 => 70,  213 => 69,  207 => 68,  198 => 67,  181 => 66,  176 => 65,  170 => 61,  168 => 60,  146 => 58,  142 => 56,  131 => 51,  128 => 50,  125 => 44,  107 => 21,  38 => 10,  155 => 58,  144 => 53,  141 => 51,  139 => 55,  135 => 47,  126 => 45,  109 => 41,  103 => 37,  101 => 32,  70 => 20,  67 => 21,  61 => 13,  47 => 9,  105 => 24,  96 => 17,  93 => 28,  83 => 18,  76 => 16,  72 => 14,  68 => 12,  50 => 10,  225 => 96,  216 => 90,  212 => 88,  205 => 84,  201 => 83,  196 => 80,  194 => 79,  191 => 78,  189 => 77,  186 => 76,  180 => 72,  172 => 67,  163 => 59,  159 => 61,  154 => 59,  147 => 55,  132 => 48,  127 => 46,  121 => 45,  118 => 44,  114 => 42,  104 => 36,  100 => 34,  78 => 21,  75 => 23,  71 => 19,  63 => 24,  58 => 9,  34 => 9,  29 => 3,  22 => 2,  91 => 10,  84 => 28,  74 => 16,  66 => 15,  55 => 15,  46 => 7,  44 => 13,  27 => 4,  25 => 5,  43 => 7,  41 => 7,  32 => 4,  28 => 3,  24 => 1,  19 => 1,  94 => 39,  88 => 6,  79 => 17,  59 => 14,  35 => 5,  31 => 4,  26 => 6,  21 => 2,  184 => 70,  178 => 71,  171 => 62,  165 => 58,  162 => 57,  157 => 60,  153 => 54,  151 => 53,  143 => 54,  138 => 51,  136 => 50,  133 => 43,  130 => 47,  122 => 43,  119 => 42,  116 => 35,  111 => 37,  108 => 31,  102 => 19,  98 => 31,  95 => 34,  92 => 33,  89 => 19,  85 => 25,  81 => 40,  73 => 24,  64 => 20,  60 => 23,  57 => 17,  54 => 10,  51 => 10,  48 => 14,  45 => 8,  42 => 6,  39 => 9,  36 => 5,  33 => 4,  30 => 3,);
    }
}
