<?php

/* MenuBundle:Default:construccion.html.twig */
class __TwigTemplate_42962720509e9d142d7ecf2ed9c95c76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "<link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
<link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/menu/css/superfish-vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" type=\"text/css\">
";
    }

    // line 9
    public function block_title($context, array $blocks = array())
    {
        echo " Ingeniería, Sistemas y Control";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<img alt=\"Logo ISC\" src=\"/uploads/images/logo.jpg\" />
<aside id=\"aside_dim\">
    <div id=\"div_dim\" style=\"position: absolute;  left: 30%;\">
        <h1 id=\"text_whiteBox\" style=\"text-align:center;\" >Ingeniería, Sistemas y Control</h1>
        <h4 id=\"text_whiteBox\" style=\"text-align:justify;\">Empresa de fabricación,
        venta y reparación de maquinaria industrial (fresadoras CNC para madera, hierro, etc). 
        La maquinaria industrial que se fabrica es totalmente adaptable a las necesidades 
        del cliente y se puede enfocar a cualquier ámbito industrial (carpintería, metalurgia, etc).
        </h4>
        <h2 id=\"text_whiteBox\" style=\"text-align:center;\" >Página web en construcción</h2>
        <h4 id=\"text_whiteBox\" style=\"text-align:justify;\">Esta página web está en construcción. 
            La página web estará pronto disponible. En caso de que quiera contactar 
        con nosotros puede hacerlo en: </h4>
        <h4 style=\"position: absolute; left: 110px; color: #0101DF; text-align: center;\">telf: (+34)676979788 <br/> email: juancq.isc@gmail.com</h4>
        <img style=\"position: absolute; left: 10px;\" alt=\"Gif Robot\" src=\"/uploads/images/arm_med_white.gif\">
    </div>
    
</aside>

";
    }

    public function getTemplateName()
    {
        return "MenuBundle:Default:construccion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 9,  77 => 25,  69 => 22,  62 => 19,  52 => 15,  40 => 11,  56 => 13,  479 => 162,  473 => 161,  468 => 158,  460 => 155,  456 => 153,  452 => 151,  443 => 149,  439 => 148,  436 => 147,  434 => 146,  429 => 144,  426 => 143,  422 => 142,  412 => 134,  408 => 132,  406 => 131,  401 => 130,  397 => 129,  392 => 126,  386 => 122,  383 => 121,  380 => 120,  378 => 119,  373 => 116,  367 => 112,  364 => 111,  361 => 110,  359 => 109,  354 => 106,  340 => 105,  336 => 103,  321 => 101,  313 => 99,  311 => 98,  308 => 97,  304 => 95,  297 => 91,  293 => 90,  284 => 89,  282 => 88,  277 => 86,  267 => 85,  263 => 84,  257 => 81,  251 => 80,  246 => 78,  240 => 77,  234 => 74,  228 => 73,  223 => 71,  219 => 70,  213 => 69,  207 => 68,  198 => 67,  181 => 66,  176 => 65,  170 => 61,  168 => 60,  146 => 58,  142 => 56,  131 => 51,  128 => 50,  125 => 44,  107 => 21,  38 => 5,  155 => 58,  144 => 53,  141 => 51,  139 => 55,  135 => 47,  126 => 45,  109 => 41,  103 => 37,  101 => 32,  70 => 20,  67 => 21,  61 => 13,  47 => 9,  105 => 24,  96 => 17,  93 => 28,  83 => 18,  76 => 16,  72 => 14,  68 => 12,  50 => 10,  225 => 96,  216 => 90,  212 => 88,  205 => 84,  201 => 83,  196 => 80,  194 => 79,  191 => 78,  189 => 77,  186 => 76,  180 => 72,  172 => 67,  163 => 59,  159 => 61,  154 => 59,  147 => 55,  132 => 48,  127 => 46,  121 => 45,  118 => 44,  114 => 42,  104 => 36,  100 => 34,  78 => 21,  75 => 23,  71 => 19,  63 => 24,  58 => 9,  34 => 9,  29 => 3,  22 => 2,  91 => 10,  84 => 28,  74 => 16,  66 => 15,  55 => 15,  46 => 7,  44 => 13,  27 => 4,  25 => 5,  43 => 7,  41 => 7,  32 => 4,  28 => 3,  24 => 1,  19 => 1,  94 => 39,  88 => 6,  79 => 17,  59 => 14,  35 => 5,  31 => 4,  26 => 6,  21 => 2,  184 => 70,  178 => 71,  171 => 62,  165 => 58,  162 => 57,  157 => 60,  153 => 54,  151 => 53,  143 => 54,  138 => 51,  136 => 50,  133 => 43,  130 => 47,  122 => 43,  119 => 42,  116 => 35,  111 => 37,  108 => 31,  102 => 19,  98 => 31,  95 => 34,  92 => 33,  89 => 19,  85 => 25,  81 => 40,  73 => 24,  64 => 20,  60 => 23,  57 => 12,  54 => 11,  51 => 10,  48 => 9,  45 => 8,  42 => 6,  39 => 9,  36 => 5,  33 => 4,  30 => 3,);
    }
}
