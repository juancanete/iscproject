<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_80a5854e10696045aa9f311338f20d8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("TwigBundle:Exception:exception.xml.twig")->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 24,  96 => 21,  93 => 20,  83 => 18,  76 => 16,  72 => 14,  68 => 12,  50 => 8,  225 => 96,  216 => 90,  212 => 88,  205 => 84,  201 => 83,  196 => 80,  194 => 79,  191 => 78,  189 => 77,  186 => 76,  180 => 72,  172 => 67,  163 => 63,  159 => 61,  154 => 59,  147 => 55,  132 => 48,  127 => 46,  121 => 45,  118 => 44,  114 => 43,  104 => 36,  100 => 34,  78 => 28,  75 => 27,  71 => 26,  63 => 24,  58 => 9,  34 => 11,  29 => 4,  22 => 2,  91 => 20,  84 => 19,  74 => 16,  66 => 15,  55 => 13,  46 => 11,  44 => 10,  27 => 4,  25 => 4,  43 => 6,  41 => 7,  32 => 4,  28 => 3,  24 => 3,  19 => 1,  94 => 39,  88 => 6,  79 => 17,  59 => 22,  35 => 7,  31 => 5,  26 => 6,  21 => 2,  184 => 70,  178 => 71,  171 => 62,  165 => 58,  162 => 57,  157 => 60,  153 => 54,  151 => 53,  143 => 54,  138 => 51,  136 => 50,  133 => 43,  130 => 47,  122 => 37,  119 => 36,  116 => 35,  111 => 32,  108 => 31,  102 => 30,  98 => 22,  95 => 31,  92 => 27,  89 => 19,  85 => 24,  81 => 40,  73 => 19,  64 => 15,  60 => 23,  57 => 14,  54 => 11,  51 => 12,  48 => 19,  45 => 8,  42 => 7,  39 => 6,  36 => 7,  33 => 5,  30 => 3,);
    }
}
